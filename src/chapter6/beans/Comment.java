package chapter6.beans;

public class Comment {
	private int userId;
	private String text;
	private int messageId;

	public void setText(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getUserId() {
		return userId;
	}

	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}

	public int getMessageId() {
		return messageId;
	}
}
